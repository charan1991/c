# .NET to do list

___

### Basics of .NET Framework
  * What is .NET framework
  * CLR, CTS, IL, JIT
  * Properties of C# language
  * Stack, Heap in C#
  
### C# OOP Concepts
  * Structures
  * Enums
  * Class -> Normal, Abstract, Static, Partial, Sealed
  * Constructor -> Default, Parameterized, Constructor Chaining, Calling Base Class Constructor
  * Destructors
  * This keyword
  * Method overloading
  * Polymorphism, Virtual methods, Overriden methods (Method overriding), Local functions (C# 7), Stack frame, Context switching in functions
  * Namespaces, (Fully qualified namespaces)
  * Assemblies, Global Assembly Cache (GAC), DLL Hell
  * Inheritance -> Single, Multilevel
  * Interfaces
  * Delegation and Composition
  * Generics, Constraints on generics, Covariance, Contravariance
  * Generic Interfaces and Classes
  
### C# core concepts and keywords in depth
  * Value type -> struct, Nullable<T> types, enum, int16, int32, int64, float, double, decimal, bool, ValueTuple
  * Reference Type -> string, class, Arrays
  * Boxing and Unboxing
  * Typecasting from value type to reference type, is keyword, as keyword, typeof keyword
  * Operators in C#, Operator overloading in C#
  * Math class, Math.Pow, Math.DivRem, Math.Floor, Math.Ceil, Math.Abstract
  * BigIntegers
  * Control statements -> if, else, if-else, if-elseif-else, switch, ternary operator, return, break, continue, throw
  * Loop statements -> for, while, do-while, foreach
  * Implicitly typed variables -> var keyword, 
  * Duck typing -> dynamic keyword, ExpandoObject, 
  * strings -> Immutability of strings, String Encoding (UTF-8, UTF-16, Unicode), string.Join, string.Replace, string.IndexOf, string.IsNullOrEmpty, string.Format, Verbatim strings, interpolated strings, StringBuilder class, StringBuilder.Append, StringBuilder.AppendLine, StringBuilder.AppendFormat 
  * static, readonly, ref, out, const keywords
  * Null coalescing operator ->'??', Safe evaluation operator '?'
  * Delegates, Anonymous functions, Lambda expressions
  * Generic delegates Func<TIn, Tout>, Action<T>, Predicate<T>
  * Parameter inference in lambda expression
  * params keyword
  * Fields, auto-implemented properties {get; set; }
  * Indexers -> Implementing custom indexers
  * Object initializers
  * Object initializers vs constructors
  * Attributes, Creating custom attributes, FlagsAttribute
  * Try, catch, finally blocks, throw vs throw new Exception, Creating custom exceptions, when clause in exceptions, Stack unwinding in exception, Exception bubbling, Checked vs unchecked exceptions
  * unsafe block
   
### C# Arrays, Collections and LINQ
  * Creating 1-D array, Array initializers, Array.Empty<T>()
  * Creating 2-D array from console input, 2-D array initializers
  * Creating n-Dimensional array from console input
  * Jagged array
  * Resizing an array
  * Cloning an array
  * Copying an array to another array -> Array.Copy
  * C# collections List<T>, Dictionary<TKey, TValue>, HashSet<T>, KeyValuePair<TKey, TValue>, Tuple<T1, T2, T3, T4, T5, T6, T7, TRest>
  * Creating extension methods
  * LINQ (Query syntax and fluent syntax)
  * .Select, .Where, .Find, .FindAll, .Contains, .Any, .All, .Aggregate, .Max, .Min, .Join, .GroupBy
  * Iterator pattern, yield return, yield break
  * Working of foreach loop
  * IEnumerator<T> and IEnumerator
  * Expression trees -> Creating expression trees
    
### C# Working with File I/O, Directories and Compression
   * System.IO.File static class
   * Streams in C# -> (FileStream and MemoryStream)
   * Applications of Streams -> (StreamWriter, StreamReader, TextWriter, TextReader)
   * System.IO.Path class -> Path.Combine method, Path.Exists
   * Directory in C# -> Directory.CreateDirectory, Directory.Exists
   * Copy one directory to another recursively using Directory class
   * FileInfo, DirectoryInfo classes
   * Compressing directories using 'ZipFile' class
  
### C# Reflection
   * FieldInfo, MethodInfo, PropertyInfo
   * Activator.CreateInstance, .BeginInvoke, .EndInvoke
   * Invoke a method using C# Reflection
  
### C# Threads
   * OS level and Kernel level threads
   * Threads in C#
   * How do local variable behave in threads
   * Thread class, Parameterized thread class.
   * ThreadStatic variables
   * Foreground and background threads
   * Put a thread in background
   * Thread.Start
   * Thread.Join (Joining threads)
   * Race condition and Deadlocks
   * Synchronization primitives
   * Monitors, Monitor.Enter, Monitor.Exit
   * Locks, Creating Locks
   * Mutex, Creating Mutexes
   * Semaphore, Creating Semaphores
   * Locks vs Mutex vs Semaphore when to use which
  
### C# Process
   * Process class -> Process.Start
   * Start notepad.exe using Process.Start
   * End process
   * Process vs threads
  
### C# Tasks
  * Tasks in C#, The Task Parallel Library (TPL)
  * Continuation passing style pattern, .ContinueWith
  * Task.Factory.StartNew, Task.Run
  * Cancelling a task
  * Cancellation tokens
  * async await keywords
  * QueueBackgroundWorkItem
  
### C# Parallel LINQ
  * Parallel LINQ
  * Parallel.ForEach

### C# HTTP Requests
  * Sending HTTP requests using System.Net.HttpClient

### C# SMTP Protocol Email
  * Sending e-mails through System.Net.SMTPClient
  
### C# Working with JSON
  * Installing Newtonsoft.Json
  * Serializing and deserializing using Newtonsoft JSON
  * LINQ to JSON -> JObject, JProperty, JToken, JArray, JContainer (when to use which)
  
### C# Working with Regular Expressions
  * Regular Expressions
  * Tokens
  * Character class
  * Quantifiers
  * Alternation
  * Capturing Groups
  * Backreferences
  * Positive Lookahead, Positive Lookbehind 
  * Negative Lookahead, Negative Lookbehind
  * Regex.Split
  * Regex.Match, Regex.Match.Groups
  * MatchCollection
  * Regex.IsMatch
  ___
  
### Design Patterns using C#
    * Singleton Pattern
    * Factory Pattern
    * Iterator Pattern
    * Dependency Injection
    * Repository and Unit of Work Pattern (using EF Core)
    * SOLID principle
  ___
  
### .NET Core
    * .NET Core, Kestrel, IIS Integration
    * .NET Core Middlewares, Create custom middleware, Order of middleware
    * .NET Core analyzing Startup.cs
    * .NET Core Static Files
    * .NET Core HostingEnvironment
    * .NET Core IOptions<T> pattern
    * .NET Core change appsettings.json b/w appsettings.{environment}.json based on ASPNETCORE_ENVIRONMENT variables
    * .NET Core HTTPContext
    * .NET Core Basic Authentication
    * .NET Core Identity, Generating JWT Tokens and handling Authentication and Authorization
 ___
    
## Connecting to MongoDB

  * What is NoSQL
  * NoSQL vs RDBMS (advantages and disadvantages)
  * Installing MongoDB.Driver nuget
  * Using MongoDB as persistence with .NET Core as service layer using Repository and Unit of Work pattern
___

# DevOps

  * What is CI/CD
  * Installing Jenkins
  * Setting up App-service and FTP Credentials
  * Installing Jenkins plugins for MsBuild and GitHub
  * What is WebHook
  * Creating Jenkins Job
  * Configuring CI step
  * Configuring Build step
  * Configuring Post Build step